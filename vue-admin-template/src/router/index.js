import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/login/register',
    component: () => import('@/views/login/register'),
    hidden: true
  },

  {
    path: '/reportdata/download',
    component: () => import('@/views/reportdata/download'),
    hidden: true
  },

  {
    path: '/changePassword',
    component: () => import('@/views/login/changePassword'),
    hidden: true
  },

  {
    path: '/login/forgetPassword',
    component: () => import('@/views/login/forgetPassword'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/index',
    children: [{
      path: 'index',
      name: 'index',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '主页' }
    }]
  },
  {
    path: '/roomchoose',
    component: Layout,
    redirect: '/roomchoose/singleRoom',
    name: 'RoomChoose',
    meta: { title: '住房申请' },
    children: [
      {
        path: 'rentalroomapp',
        name: 'rentalroomapp',
        component: () => import('@/views/roomchoose/rentalroomapp'),
        meta: { title: '公租房申请' }
      },
      {
        path: 'roomapp',
        name: 'roomapp',
        component: () => import('@/views/roomchoose/roomapp'),
        meta: { title: '宿舍申请' }
      },
      {
        path: 'index',
        name: 'selfuse',
        component: () => import('@/views/selfuse/index'),
        meta: { title: '自用办公' }
      }
    ]
  },

  {
    path: '/rentingout',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'rentingout',
        hidden: true,
        component: () => import('@/views/rentingout/index'),
        meta: { title: '合同' }
      }
    ]
  },
  {
    path: '/stuffquarter',
    component: Layout,
    redirect: '/stuffquarter/stuffapply',
    name: 'stuffquarter',
    meta: { title: '员工宿舍' },
    children: [
      {
        path: 'quarterrepair',
        name: 'quarterrepair',
        component: () => import('@/views/stuffquarter/quarterrepair'),
        meta: { title: '宿舍报修' }
      },
      {
        path: 'index',
        name: 'hydropower',
        component: () => import('@/views/hydropower/index'),
        meta: { title: '水电模块' }
      }
    ]
  },
  {
    path: '/intoandexit',
    component: Layout,
    name: 'intoandexit',
    meta: { title: '进场与退租' },
    children: [
      {
        path: 'into',
        name: 'into',
        component: () => import('@/views/intoandexit/into'),
        meta: { title: '进场模块' }
      },
      {
        path: 'exit',
        name: 'exit',
        component: () => import('@/views/intoandexit/exit'),
        meta: { title: '退租模块' }
      }
    ]
  },
  {
    path: '/balancecenter',
    component: Layout,
    redirect: '/balancecenter/inspection',
    name: 'Balancecenter',
    meta: { title: '结算中心' },
    children: [
      {
        path: 'inspection',
        name: 'Inspection',
        component: () => import('@/views/balancecenter/inspection'),
        meta: { title: '物品查验' }
      },
      {
        path: 'confirmation',
        name: 'Confirmation',
        hidden: true,
        component: () => import('@/views/balancecenter/confirmation'),
        meta: { title: '物管员确认' }
      }
    ]
  },
  {
    path: '/reportdata',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'reportdata',
        component: () => import('@/views/reportdata/index'),
        meta: { title: '报表数据' }
      }
    ]
  },
  {
    path: '/admin',
    component: Layout,
    name: 'admin',
    children: [
      {
        path: 'roommessage',
        name: 'roommessage',
        hidden: true,
        component: () => import('@/views/admin/roommessage'),
        meta: { title: '宿舍申请管理' }
      },
      {
        path: 'checkOutMessage',
        name: 'checkOutMessage',
        hidden: true,
        component: () => import('@/views/admin/checkOutMessage'),
        meta: { title: '退场信息管理' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export const asyncRoutes = []
export default router
