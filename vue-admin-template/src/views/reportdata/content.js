import axios from 'axios'
import Cookies from 'js-cookie'

const title = 'Plans for the Next Iteration of Vue.js'

const content = `<div v-else class="border" style="width: 70%;margin: auto;padding: 0">
      <div class="displayFlex"><h2>缴费单</h2></div>
      <div class="displayFlex">
        <div class="border Flex item">姓名：{{ payTable[0].payername }}</div>
        <div class="border Flex item">账号：{{ wepayment.payerId }}</div>
        <div class="border Flex item">房间号：{{ wepayment.payRoomId }}</div>
      </div>
      <div class="displayFlex">
        <div class="border Flex item">水费：{{ wepayment.waterPrice }}</div>
        <div class="border Flex item">电费：{{ wepayment.electronPrice }}</div>
      </div>
      <div class="displayFlex">
        <div class="border Flex item">出账时间：{{ wepayment.payStartDate }}</div>
        <div class="border Flex item">缴费截止：{{ wepayment.payEndDate }}</div>
      </div>
      <div class="border" style="height: 100px">
        <div>备注：{{ wepayment.remark }}</div>
      </div>
      <div class="border" style="text-align: right">
        <el-radio v-model="radio" label="微信">微信</el-radio>
        <el-radio v-model="radio" label="支付宝">支付宝</el-radio>
        <el-button type="primary" @click="onSubmit">支付</el-button>
      </div>
    </div>`

const data = {
  title,
  content,
  payTable: []
}

export default {
  data,
  methods: {
    queryIsPay() {
      const that = this
      axios.get('http://localhost:8080/wepay/personalExpensesInfo',
        {
          params: {
            roomId: Cookies.get('bedId'),
            status: 1
          }
        }).then(function(result) {
        if (result.data.data !== null) {
          that.payTable = result.data.data
        }
      })
    }
  }
}
