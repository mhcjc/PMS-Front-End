import request from '@/utils/request'

export function testMap() {
  return request({
    url: '/test/map',
    method: 'post',
    baseURL: 'http://localhost:8080/'
  })
}
