import request from '@/utils/request'
import Qs from 'qs'

export function selectGoodsInpection(data) {
  return request({
    url: '/GoodInspection/queryroom',
    method: 'get',
    baseURL: 'http://localhost:8080',
    data: data,
    transformRequest: [function(data) {
      // 对 data 进行任意转换处理
      return Qs.stringify(data)
    }],
    headers: { 'Content-Type': 'application/json;charset=UTF-8' }
  })
}
