import request from '@/utils/request'

export function queryIndex(data) {
  return request({
    url: '/house/houseQuery',
    method: 'post',
    baseURL: 'http://localhost:8080/',
    headers: { 'Content-Type': 'application/json;charset=UTF-8' }
  })
}
export function queryIndex1() {
  return request({
    url: '/index/queryIndex',
    method: 'post',
    baseURL: 'http://localhost:8080/',
    headers: { 'Content-Type': 'application/json;charset=UTF-8' }
  })
}
/*
export function hello() {
  return request({
    url: '/test/map',
    method: 'post',
    baseURL: 'http://localhost:8080/',
    data: data
  })
}*/
